$( document ).ready(function() {

  window.sr = ScrollReveal();
  sr.reveal('.homepage-dish-card', { duration: 2000 }, 200);
  sr.reveal('.faq',{ duration: 500 });

  $('.parallax-1').parallax({imageSrc: 'assets/img/parallax-1.jpg'});
  $('.parallax-2').parallax({imageSrc: 'assets/img/parallax-2.jpg'});

  if($(window).width() > 991) {

    var recipeHeight = $('.recipe-card__image-inner').parent().height();

    $('.recipe-card__image-inner').css('height', recipeHeight);
    console.log($('.recipe-card-outer').height());
    var recipeMarginTop = ($(window).height() - $('.recipe-card-outer').height() ) / 2;

    $('.recipe-card').css('margin-top', recipeMarginTop);
  }

  if($(window).width() < 991) {
    $('.receipt-body').css('height', $(window).height() - 129);
  }
  else {
    $('.leftpane').css('min-height', $(window).height());
  }

  // Get started!
  $('#select-month').selectize({
		create: true,
		sortField: {
			field: 'text',
			direction: 'asc'
		},
		dropdownParent: 'body'
	});

  $('#select-year').selectize({
		create: true,
		sortField: {
			field: 'text',
			direction: 'asc'
		},
		dropdownParent: 'body'
	});


  $('#select-city').selectize({
		create: true,
		sortField: {
			field: 'text',
			direction: 'asc'
		},
		dropdownParent: 'body'
	});

  $('#select-province').selectize({
		create: true,
		sortField: {
			field: 'text',
			direction: 'asc'
		},
		dropdownParent: 'body'
	});


  $('#select-suburb').selectize({
		create: true,
		sortField: {
			field: 'text',
			direction: 'asc'
		},
		dropdownParent: 'body'
	});

  if(window.location.hash) {
    var elem = $('#_' + window.location.hash.replace('#', ''));
    $('html, body').animate({
        scrollTop: elem.offset().top
    }, 2000);
  }


});
